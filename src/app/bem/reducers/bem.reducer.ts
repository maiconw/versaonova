import { EntityState, createEntityAdapter, EntityAdapter } from '@ngrx/entity';
import { BemActionTypes, BemActions } from '../actions/bem.actions';
import { Bem } from '../models/bem.model';

export interface State extends EntityState<Bem> {
  selectedBemId: string | null;
  loading: boolean;
}


export const adapter: EntityAdapter<Bem> = createEntityAdapter<Bem>({
  selectId: (bem: Bem) => bem.id,
});

export const initialState: State = adapter.getInitialState({
  selectedBemId: null,
  loading: false,
});

export function reducer(
  state: State = initialState,
  action: BemActions
): State {
  switch (action.type) {
    case BemActionTypes.LoadBens: {
      return {
        ...state,
        loading: true,
      };
    }

    case BemActionTypes.LoadSuccessBens: {
      return adapter.addAll(action.payload, state);
    }

    default:
      return state;
  }
}

export const getSelectedBemId = (state: State) => state.selectedBemId;

export const {
  // select the array of user ids
  selectIds: getBemIds,

  // select the dictionary of user entities
  selectEntities: getBemEntities,

  // select the array of users
  selectAll: getAllBem,

  // select the total user count
  selectTotal: getBemTotal
} = adapter.getSelectors();
