/*
    MIT License

    Copyright (c) 2017 Temainfo Sistemas

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/
import {
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
} from '@ngrx/store';
import * as fromBem from './bem.reducer';

export interface BemState {
  collection: fromBem.State;
}

export const reducers: ActionReducerMap<BemState> = {
  collection: fromBem.reducer
};


export const getBemState = createFeatureSelector<BemState>('bem');

export const getCollectionState = createSelector(
  getBemState,
  (state: BemState) => state.collection
);

export const getBemIds = createSelector(getCollectionState, fromBem.getBemIds);
export const getBemEntities = createSelector(getCollectionState, fromBem.getBemEntities);
export const getAllBem = createSelector(getCollectionState, fromBem.getAllBem);
export const getBemTotal = createSelector(getCollectionState, fromBem.getBemTotal);
export const getCurrentBemId = createSelector(getCollectionState, fromBem.getSelectedBemId);

export const getCurrentBem = createSelector(
  getBemEntities,
  getCurrentBemId,
  (bemEntities, bemId) => bemEntities[bemId]
);
