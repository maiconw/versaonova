import { Injectable } from '@angular/core';
import { RestService } from '../../shared/third-parts/featherjs/rest.service';
import { SocketService } from '../../shared/third-parts/featherjs/socket.service';

@Injectable()
export class BemService {

  constructor( private _rest: RestService, private _socket: SocketService) {}

  getAllBens() {
    const collectionStringIDS: Array<string> = this._socket.getApp().get('user')['bens'];
    return this._socket.getService('bem').find({
        query: {
          id: { $in: collectionStringIDS },
          $limit: 50
        }
      });
  }
}

