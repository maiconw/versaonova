import {Injectable} from '@angular/core';

import * as LogRocket from 'logrocket';
import { Observable } from 'rxjs/Observable';
import { Usuario } from '../../../login/models/usuario';

@Injectable()
export class LogRocketService {

  constructor() {
   this.initLogRocket();
  }

  static getLogRocketInstance() {
    return LogRocket;
  }

  identifyUser(usuario$:  Observable<Usuario>) {
    usuario$.subscribe((usuario) => {

      if( usuario === null ) { return; }

      LogRocket.identify( usuario.id, {
        name: usuario.nome + ' ' + usuario.sobrenome,
        email: usuario.email,

        // Add your own custom user variables here, ie:
        subscriptionType: 'pro'
      });
    });
  }

  private initLogRocket() {
    LogRocket.init('vq5tqe/otiseg-test');
  }
}
