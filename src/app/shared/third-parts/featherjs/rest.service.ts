import { Injectable } from '@angular/core';
import { FeathersService } from './feathers.service';
import { environment } from '../../../../environments/environment';

import * as superagent from 'superagent';
import * as feathers from '@feathersjs/client';
import * as rest from '@feathersjs/rest-client';
import * as authentication from '@feathersjs/authentication-client';

@Injectable()
export class RestService extends FeathersService {

  constructor(
  ) {
    super();
    this._app = feathers()
      .configure(rest(environment.serverHost).superagent(superagent))
      .configure(authentication({ storage: window.localStorage }));

    if (window.localStorage.getItem('feathers-jwt')) {
      this._app.set('token', window.localStorage.getItem('feathers-jwt'));
    }
  }
}
