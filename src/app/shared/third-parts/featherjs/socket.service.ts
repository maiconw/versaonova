import { Injectable } from '@angular/core';
import { Frota } from '../../../frota/models/frota.model';
import { FeathersService } from './feathers.service';
import { environment } from '../../../../environments/environment';

import * as io from 'socket.io-client';
import * as feathers from '@feathersjs/client';
import * as socketio from '@feathersjs/socketio-client';
import * as authentication from '@feathersjs/authentication-client';

@Injectable()
export class SocketService extends FeathersService {
  public socket: SocketIOClient.Socket;

  constructor(
  ) {
    super();


    this.socket = io(environment.serverHost, {
      // transports: ['websocket'],
      reconnectionAttempts: 10,
      reconnectionDelay: 10000
    });


    this.socket.on('created', ( data: Frota )=> {
      console.log(data);
    });


    this._app = feathers()
      .configure(socketio(this.socket))
      .configure(authentication({ storage: window.localStorage }));

    this.authenticateIfPossible();

    this.socket.io.on('reconnect', () => {
      console.log('--> reconnect');
      // this._app.authenticate();
    });

    this.socket.io.on('reconnect_attempt', (attemptNumber) => {
      console.log('Tentativa de Conexao: ' + attemptNumber);
      return;
    });

    this.socket.io.on('reconnecting', (attemptNumber) => {
      console.log('reconnecting: Tentativa de Conexao: ' + attemptNumber);
      return;
    });

    this.socket.io.on('reconnect_failed', () => {
      console.log('As tentativas de Reconecao Falharam: ');
      return;
    });

    this.socket.io.on('error', (error) => {
      console.log('Erro ao conectar com o servidor remoto: ' + error);
      return;
    });

    this.socket.io.on('disconnect', (reason) => {
      console.log('Conexao com servidor foi perdida: ' + reason);
      return;
    });


    this.socket.io.on('connect_error', (error) => {
      console.log('connect_error ao conectar com o servidor remoto: ' + error);
      return;
    });

    this.socket.io.on('connect_timeout', (timeout) => {
      console.log('Tempo Limite de conexao: ' + timeout);
      return;
    });
    // If the transport changes, you have to authenticate again.
    this.socket.io.on('upgrade', transport => {
      console.log('>> transport changed [socket.service]');
      this.authenticateIfPossible();
    });
  }

  private authenticateIfPossible() {

    if (window.localStorage.getItem('feathers-jwt')) {
      // this._app.set('token', window.localStorage.getItem('feathers-jwt'));
      // console.log(
      //   window.localStorage.getItem('feathers-jwt')
      // );
      // console.log(
      //   this._app.get('token')
      // );
      //
      // // If we have a token, we can authenticate
      // this._app.authenticate().then(res => {
      //   console.log(res);
      //   console.log('Authenticated done');
      // }).catch((error) => {
      //   console.error('Error ' + error);
      // });
    }
  }

  public on(trigger: string, callback: Function) {
   return this.socket.on(trigger, callback);
  }
}
