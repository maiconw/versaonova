import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { tap, map, exhaustMap, switchMap } from 'rxjs/operators';
import { LoadBens } from '../../bem/actions/bem.actions';
import { LoadComandos } from '../../comando/actions/collection.actions';
import { LoadFrotas } from '../../frota/actions/frota.actions';

import * as fromAuth from '../../login/reducers';
import { LoadTracker } from '../../tracker/actions/tracker.actions';

import {
  Login,
  LoginSuccess,
  LoginFailure,
  AuthActionTypes,
} from '../actions/auth.actions';
import { Authenticate } from '../models/authenticate';
import { AuthService } from '../resources/auth.service';

@Injectable()
export class AuthEffects {

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router,
    private store: Store<fromAuth.AuthState>
  ) {}

  @Effect()
  login$ = this.actions$.pipe(
    ofType( AuthActionTypes.Login ),
    map(( action: Login) => action.payload ),
    exhaustMap(( auth: Authenticate ) =>
      this.authService
        .authenticate( auth.username, auth.password )
        .then(res => {
          return this.authService.verifyJWT(res.accessToken);
        })
        .then(payload => {
          return this.authService.findUsuario(payload.userId);
        })
        .then(user => {
          return this.authService.setUsuario(user);
        })
        .then( () => {
          const usuario = this.authService.getUsuario();
          this.router.navigate(['/main'])
          return new LoginSuccess({ usuario } );
        })
        .catch( error => new LoginFailure(error.message) )
    )
  );

  @Effect()
  loginReautenticate = this.actions$.pipe(
    ofType( AuthActionTypes.LoginReAutenticate ),
    exhaustMap((  ) =>
      this.authService
        .reauthenticate( )
        .then( usuario => new LoginSuccess({ usuario } ))
        .catch( error => new LoginFailure(error.message) )
    )
  );

  @Effect()
  loginSuccess$ = this.actions$.pipe(
    ofType(AuthActionTypes.LoginSuccess),
    switchMap(() => [
      new LoadBens(),
      new LoadFrotas(),
      new LoadTracker(),
      new LoadComandos()
    ]),
  );

  @Effect({ dispatch: false })
  loginRedirect$ = this.actions$.pipe(
    ofType(AuthActionTypes.LoginRedirect, AuthActionTypes.Logout),
    tap(authed => {
      this.authService.logout();
      this.router.navigate(['/login']);
    })
  );
}
