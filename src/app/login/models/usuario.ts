export interface Usuario {
  id: string;

  apelido: string;
  ativo: boolean;
  bens: [any];
  email: string;
  nome: string;
  password: string;
  sobrenome: string;
  username: string;
}
