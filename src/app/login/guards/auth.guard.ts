import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as Auth from '../../login/actions/auth.actions';
import * as fromAuth from '../../login/reducers';
import { AuthService } from '../resources/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor( private authService: AuthService, private store: Store<fromAuth.AuthState> ) {
  }

  canActivate(): Observable<boolean> {
    return this.authService.isLogin() || this.authService.reauthenticate()
      .then(res => this.authService.verifyJWT(res.accessToken))
      .then(payload => this.authService.findUsuario(payload.userId))
      .then(user =>  this.authService.setUsuario( user ) )
      .then(res => {
        this.store.dispatch( new Auth.LoginSuccess({usuario: res.settings.user}));
        return true;
      })
      .catch(err => {
        this.store.dispatch( new Auth.LoginRedirect() );
      });
  }
}
