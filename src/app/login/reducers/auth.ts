import { AuthActions, AuthActionTypes } from '../actions/auth.actions';
import { Usuario } from '../models/usuario';

export interface State {
  loggedIn: boolean;
  usuario: Usuario | null;
}

export const initialState: State = {
  loggedIn: false,
  usuario: null,
};

export function reducer(state = initialState, action: AuthActions): State {
  switch (action.type) {
    case AuthActionTypes.LoginSuccess: {
      return {
        ...state,
        loggedIn: true,
        usuario: action.payload.usuario,
      };
    }

    case AuthActionTypes.Logout: {
      return initialState;
    }

    default: {
      return state;
    }
  }
}

export const getLoggedIn = (state: State) => state.loggedIn;
export const getUser = (state: State) => state.usuario;
