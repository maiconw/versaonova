import { Injectable } from '@angular/core';
import { RestService } from '../../shared/third-parts/featherjs/rest.service';
import { SocketService } from '../../shared/third-parts/featherjs/socket.service';


@Injectable()
export class AuthService {
  constructor(
    private _rest: RestService,
    private _socket: SocketService
  ) {}

  authenticate(email: string, password: string) {
   return this._socket.getApp().authenticate({
      strategy: 'local',
      email: email,
      password: password
    });
  }

  reauthenticate() {
    return this._socket.getApp().authenticate();
  }

  findUsuario(usuarioID) {
    return this._socket.getApp().service('usuario').get(usuarioID);
  }

  getUsuario() {
    return this._socket.getApp().get('user');
  }

  setUsuario( user ) {
   return this._socket.getApp().set('user', user);
  }

  getToken() {
    return this._socket.getApp().get('token');
  }

  verifyJWT(accessToken) {
    return this._socket.getApp().passport.verifyJWT(accessToken);
  }


  isLogin() {
    return ( (!!this.getUsuario()) && ( window.localStorage.getItem('feathers-jwt') !== '' ));
  }

  logout() {
    return this._rest.getApp().logout();
  }
}
