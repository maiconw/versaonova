import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Authenticate } from '../../models/authenticate';

@Component({
  selector: 'ot-formcard',
  templateUrl: './formcard.component.html',
  styleUrls: ['./formcard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormcardComponent implements OnInit {

  @Input()
  set pending(isPending: boolean) {
    if (isPending) {
      this.form.disable();
    } else {
      this.form.enable();
    }
  }

  @Input() errorMessage: string;

  @Output() submitted = new EventEmitter<Authenticate>();

  public form: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });


  constructor() { }

  ngOnInit() {}

  submit() {
    if (this.form.valid) {
      this.submitted.emit(this.form.value);
    }
  }

}
