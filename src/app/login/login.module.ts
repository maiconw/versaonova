import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { FormcardComponent } from './components/formcard/formcard.component';
import { LayoutComponent } from './components/layout/layout.component';
import { LoginComponent } from './containers/login.component';

import { MaterialModule } from '../shared/third-parts/material';
import { AuthEffects } from './effects/auth.effects';
import { AuthGuard } from './guards/auth.guard';
import { reducers } from './reducers';
import { AuthService } from './resources/auth.service';

export const COMPONENTS = [
  FormcardComponent,
  LayoutComponent,
  LoginComponent,
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: LoginComponent }]),
    StoreModule.forFeature('login', reducers ),
    EffectsModule.forFeature([ AuthEffects ]),
  ],
  declarations: [ COMPONENTS ],
  exports: [ LoginComponent ],
})
export class LoginModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: LoginModule,
      providers: [ AuthService ],
    };
  }
}
