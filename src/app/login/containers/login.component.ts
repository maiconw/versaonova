import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Authenticate } from '../models/authenticate';

import * as fromAuth from '../reducers';
import * as Auth from '../actions/auth.actions';

@Component({
  selector: 'ot-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public pending$: Observable<Boolean>;

  public errors$: Observable<any>;

  constructor( private store: Store<fromAuth.AuthState> ) { }

  ngOnInit() {
    this.pending$ = this.store.pipe(select(fromAuth.getLoginPagePending));
    this.errors$ = this.store.pipe(select(fromAuth.getLoginPageError));
  }

  onSubmit($event: Authenticate) {
    this.store.dispatch(new Auth.Login($event));
  }

}
