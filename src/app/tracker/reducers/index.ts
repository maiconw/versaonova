/*
    MIT License

    Copyright (c) 2017 Temainfo Sistemas

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/
import {
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
} from '@ngrx/store';
import { Bem } from '../../bem/models/bem.model';
import { Tracker } from '../models/tracker.model';
import * as fromTracker from './tracker.reducer';
import * as fromMain from '../../main/reducers';

export interface TrackerState {
  collection: fromTracker.State;
}

export const reducers: ActionReducerMap<TrackerState> = {
  collection: fromTracker.reducer
};


export const getTrackerState = createFeatureSelector<TrackerState>('tracker');

export const getCollectionState = createSelector(
  getTrackerState,
  (state: TrackerState) => state.collection
);

export const getTrackerIds = createSelector(getCollectionState, fromTracker.getTrackerIds);
export const getTrackerEntities = createSelector(getCollectionState, fromTracker.getTrackerEntities);
export const getAllTracker = createSelector(getCollectionState, fromTracker.getAllTracker);
export const getTrackerTotal = createSelector(getCollectionState, fromTracker.getTrackerTotal);
export const getCurrentTrackerId = createSelector(getCollectionState, fromTracker.getSelectedTrackerId);

export const getCurrentTracker = createSelector(
  getTrackerEntities,
  getCurrentTrackerId,
  (trackerEntities, trackerId) => trackerEntities[trackerId]
);

export const getTrackerBySelectedBem = createSelector(fromMain.getSelectedBem, getTrackerEntities, (selectedBem: Bem, trackerEntities) => {
  if (selectedBem && trackerEntities) {
    return trackerEntities[selectedBem.id];
  }else {
    return null;
  }
});
