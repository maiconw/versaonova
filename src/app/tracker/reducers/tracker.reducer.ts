import { EntityState, createEntityAdapter, EntityAdapter } from '@ngrx/entity';
import { TrackerActionTypes, TrackerActions } from '../actions/tracker.actions';
import { Tracker } from '../models/tracker.model';

export interface State extends EntityState<Tracker> {
  selectedTrackerId: string | null;
  loading: boolean;
}


export const adapter: EntityAdapter<Tracker> = createEntityAdapter<Tracker>({
  selectId: (tracker: Tracker) => tracker.bem_id
});

export const initialState: State = adapter.getInitialState({
  selectedTrackerId: null,
  loading: false,
});

export function reducer(
  state: State = initialState,
  action: TrackerActions
): State {

  switch (action.type) {
    case TrackerActionTypes.LoadTracker: {
      return {
        ...state,
        loading: true,
      };
    }

    case TrackerActionTypes.LoadSuccessTracker: {
      return adapter.addAll(action.payload, state);
    }

    default:
      return state;
  }
}

export const getSelectedTrackerId = (state: State) => state.selectedTrackerId;

export const {
  // select the array of user ids
  selectIds: getTrackerIds,

  // select the dictionary of user entities
  selectEntities: getTrackerEntities,

  // select the array of users
  selectAll: getAllTracker,

  // select the total user count
  selectTotal: getTrackerTotal
} = adapter.getSelectors();
