/*
    MIT License

    Copyright (c) 2017 Temainfo Sistemas

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { switchMap } from 'rxjs/operators';
import { LoadFailTracker, LoadSuccessTracker, TrackerActions, TrackerActionTypes } from '../actions/tracker.actions';
import { Tracker } from '../models/tracker.model';
import { TrackerService } from '../resources/tracker.service';

@Injectable()
export class TrackerEffects {

  constructor(private actions$: Actions, private trackerService: TrackerService) {}

  @Effect()
  loadTracker: Observable<TrackerActions> = this.actions$.pipe(
    ofType(TrackerActionTypes.LoadTracker),
    switchMap(() =>
      this.trackerService.getAllTracker()
        .then((trackers: Tracker[]) => {
          const collections = trackers.map((tracker) => tracker['reduction']);
          return new LoadSuccessTracker(collections);
        })
        .catch( (error) => new LoadFailTracker(error.message))
    )
  );
}
