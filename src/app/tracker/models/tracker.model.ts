export interface Tracker {
  id: string;
  bem_id: string;
  altitude: number;
  direcao: number;
  dt_evento: number;
  dt_gps: number;
  id_msg: string;
  informacoes: any;
  latitude: number;
  longitude: number;
  tipo: string;
  velocidade: number;
}
