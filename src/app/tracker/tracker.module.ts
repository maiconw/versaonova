import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { TrackerEffects } from './effects/tracker.effects';
import { TrackerService } from './resources/tracker.service';

import * as fromTracker from './reducers';

@NgModule({
  imports: [
    StoreModule.forFeature('tracker', fromTracker.reducers),
    EffectsModule.forFeature([TrackerEffects])
  ],
  providers: [TrackerService]
})
export class TrackerModule { }
