import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Bem } from '../../../bem/models/bem.model';
import { Tracker } from '../../../tracker/models/tracker.model';

@Component({
  selector: 'ot-info-window-content',
  templateUrl: './info-window-content.component.html',
  styleUrls: ['./info-window-content.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InfoWindowContentComponent implements OnInit {

  @Input() selectedBem: Bem;

  @Input() tracker: Tracker;

  constructor() { }

  ngOnInit() {
  }

}
