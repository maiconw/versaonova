import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Bem } from '../../bem/models/bem.model';
import { Tracker } from '../../tracker/models/tracker.model';

import * as fromTracker from '../../tracker/reducers';
import * as fromMain from '../../main/reducers';
import * as fromMap from '../reducers';
import { Position } from '../models/position.model';

@Component({
  selector: 'ot-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapComponent implements OnInit {

  public trackerBySelectedBem$: Observable<Tracker>;

  public selectedBem$: Observable<Bem>;

  public mapPositions$: Observable<Position>;

  constructor( private store: Store<fromTracker.TrackerState>) {
    this.trackerBySelectedBem$ = this.store.pipe(select(fromTracker.getTrackerBySelectedBem));
    this.selectedBem$ = this.store.pipe(select(fromMain.getSelectedBem));
    this.mapPositions$ = this.store.pipe(select(fromMap.getPositions));
  }

  ngOnInit() {}

}
