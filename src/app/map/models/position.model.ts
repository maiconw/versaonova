export interface Position {
  longitude: number;
  latitude: number;
  zoom: number;
}
