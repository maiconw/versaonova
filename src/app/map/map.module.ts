import { AgmCoreModule } from '@agm/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { MaterialModule } from '../shared/third-parts/material';
import { MapComponent } from './containers/map.component';
import { InfoWindowContentComponent } from './components/info-window-content/info-window-content.component';

import * as fromMap from './reducers';

export const COMPONENTS = [
  MapComponent,
  InfoWindowContentComponent
];

@NgModule({
  imports: [
    CommonModule,
    AgmCoreModule,
    MaterialModule,
    StoreModule.forFeature('map', fromMap.reducers),
    EffectsModule.forFeature([])
  ],
  declarations: [ COMPONENTS ],
  exports: [ MapComponent ]
})
export class MapModule { }
