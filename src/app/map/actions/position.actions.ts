import { Action } from '@ngrx/store';
import { Position } from '../models/position.model';

export enum PositionActionsTypes  {
  SetPositionsMap = '[Map] Posiciona a visualização do Map'
}

export class SetPositionsMap implements Action {
  readonly type = PositionActionsTypes.SetPositionsMap;

  constructor(public payload: Position) {}
}

export type PositionActions = | SetPositionsMap;
