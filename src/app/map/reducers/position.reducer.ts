import { PositionActions, PositionActionsTypes } from '../actions/position.actions';
import { Position } from '../models/position.model';

export interface MapState {
  positions: Position | null;
}

export const initialState: MapState = {
  positions: { latitude : -24.282022, longitude : -53.8419715, zoom : 15 }
};

export function reducer(
  state = initialState,
  action: PositionActions
): MapState {
  switch (action.type) {

    case PositionActionsTypes.SetPositionsMap: {
      return {
        positions: action.payload,
      };
    }

    default:
      return state;
  }
}

export const getPositions = (state: MapState) => state.positions;
