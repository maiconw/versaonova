import {
  ActionReducerMap,
  createFeatureSelector,
  createSelector
} from '@ngrx/store';

import * as fromPositions from '../reducers/position.reducer';

export interface MapState  {
  positions: fromPositions.MapState;
}

export const reducers: ActionReducerMap<MapState> = {
  positions: fromPositions.reducer
};

export const getMapState = createFeatureSelector<MapState>('map');


// Layout Selectors
export const getPositionState = createSelector(
  getMapState,
  (state: MapState) => state.positions
);

export const getPositions = createSelector(
  getPositionState,
  fromPositions.getPositions
);
