import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { AgmCoreModule } from '@agm/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { LoginComponent } from '../login/containers/login.component';
import { AuthGuard } from '../login/guards/auth.guard';
import { MainComponent } from '../main/containers/main.component';
import { MainModule } from '../main/main.module';

import { MaterialModule } from '../shared/third-parts/material';
import { environment } from '../../environments/environment';
import { SocketService } from '../shared/third-parts/featherjs/socket.service';
import { RestService } from '../shared/third-parts/featherjs/rest.service';
import { LogRocketService } from '../shared/third-parts/logrocket/logrocket.service';
import { AppComponent } from './containers/app.component';
import { LoginModule } from '../login/login.module';
import { reducers, metaReducers } from './reducers';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,

    // Angular GoogleMaps Inicalization
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDQrx0oAy0TrMK3Zr77-tkixDA5TceS9oM',
      language: 'pt-BR',
      region: 'BR'
    }),

    /**
     * StoreModule.forRoot is imported once in the root module, accepting a reducer
     * function or object map of reducer functions. If passed an object of
     * reducers, combineReducers will be run creating your application
     * meta-reducer. This returns all providers for an @ngrx/store
     * based application.
     */
    StoreModule.forRoot(reducers, { metaReducers }),

    /**
     * EffectsModule.forRoot() is imported once in the root module and
     * sets up the effects class to be initialized immediately when the
     * application starts.
     *
     * See: https://github.com/ngrx/platform/blob/master/docs/effects/api.md#forroot
     */
    EffectsModule.forRoot([]),

    /**
     * Store devtools instrument the store retaining past versions of state
     * and recalculating new states. This enables powerful time-travel
     * debugging.
     *
     * To use the debugger, install the Redux Devtools extension for either
     * Chrome or Firefox
     *
     * See: https://github.com/zalmoxisus/redux-devtools-extension
     */
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production // Restrict extension to log-only mode
    }),

    // Modules Inicialization
    RouterModule.forRoot([

      { path: '', redirectTo: 'login', pathMatch: 'full' },
      { path: 'login', component: LoginComponent },
      { path: 'main', component: MainComponent },
      { path: '**', redirectTo: 'main' },
    ], { useHash: true, enableTracing: false }),

    MainModule,
    LoginModule.forRoot(),
  ],
  providers: [
    LogRocketService,
    RestService,
    SocketService,
    AuthGuard,
  ],
  bootstrap: [ AppComponent ],
  declarations: [ AppComponent ],
})
export class AppModule { }
