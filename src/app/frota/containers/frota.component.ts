import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Frota } from '../models/frota.model';
import * as fromFrota from '../reducers';

@Component({
  selector: 'ot-frota',
  templateUrl: './frota.component.html',
  styleUrls: ['./frota.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FrotaComponent implements OnInit {

  frota$: Observable<Frota[]>;

  rows = [
    { name: 'Austin', gender: 'Male', company: 'Swimlane' },
    { name: 'Dany', gender: 'Male', company: 'KFC' },
    { name: 'Molly', gender: 'Female', company: 'Burger King' },
  ];
  constructor( private store: Store<fromFrota.FrotaState>) {
    this.frota$ = this.store.pipe(select(fromFrota.getAllFrota));
  }

  ngOnInit() {
  }

}
