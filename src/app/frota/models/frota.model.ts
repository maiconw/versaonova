export interface Frota {
  id: string;
  descricao: string;
  ativo: boolean;
  bens: any;
}
