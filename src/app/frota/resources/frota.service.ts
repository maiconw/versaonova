import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RestService } from '../../shared/third-parts/featherjs/rest.service';
import { SocketService } from '../../shared/third-parts/featherjs/socket.service';
import { Frota } from '../models/frota.model';

@Injectable()
export class FrotaService {
  constructor( private _rest: RestService,
               private _socket: SocketService
  ) {};

  observe(trigger): Observable<any> {
    return new Observable<any>((observer)=> {
      this._socket.getService('frota').on(trigger, ( data: Frota )=> {
        observer.next(data)
      })
    })
  }

  getAllFrota() {
    const usuario_id = this._socket.getApp().get('user')['id'];
    return this._socket.getService('frota').find({query: {usuario_id: usuario_id}});
  }
}
