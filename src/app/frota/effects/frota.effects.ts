import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { defer } from 'rxjs/observable/defer';
import { of } from 'rxjs/observable/of';
import { switchMap, map } from 'rxjs/operators';
import { FrotaService } from '../resources/frota.service';
import {
  FrotaActions, FrotaActionTypes, LoadFail, LoadFrotas, LoadSuccessFrotas, AppendFrota
} from '../actions/frota.actions';
import { Frota } from '../models/frota.model';


@Injectable()
export class FrotaEffects {

  constructor(private actions$: Actions, private frotaService: FrotaService) {}

  init$: Observable<FrotaActions> = defer(() => of(null)).pipe(
    switchMap(() =>
      this.frotaService.getAllFrota()
        .then((frotas: Frota[]) => new LoadSuccessFrotas(frotas['data']))
        .catch( (error) => new LoadFail(error.message) )
    )
  );

  @Effect()
  loadFrota: Observable<FrotaActions> = this.actions$.pipe(
    ofType(FrotaActionTypes.LoadFrotas),
    switchMap(() =>
      this.frotaService.getAllFrota()
        .then((frotas: Frota[]) => new LoadSuccessFrotas(frotas['data']))
        .catch( (error) => new LoadFail(error.message) )
    )
  );

  @Effect()
  appendFrota: Observable<FrotaActions> = this.frotaService.observe('created').pipe(
    map((frota: Frota) => {
      return new AppendFrota(frota)
    })
  )
}
