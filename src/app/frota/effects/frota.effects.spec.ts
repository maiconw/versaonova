import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs/Observable';

import { FrotaEffects } from './frota.effects';

describe('FrotaService', () => {
  let actions$: Observable<any>;
  let effects: FrotaEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        FrotaEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(FrotaEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
