import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Frota } from '../models/frota.model';

export enum FrotaActionTypes {
  LoadFrotas = '[Frota] Load Frotas',
  LoadSuccessFrotas = '[Frota] Load Success Frota',
  LoadFail = '[Frota] Load Fail Frota',
  AddFrota = '[Frota] Add Frota',
  UpsertFrota = '[Frota] Upsert Frota',
  AddFrotas = '[Frota] Add Frotas',
  UpsertFrotas = '[Frota] Upsert Frotas',
  UpdateFrota = '[Frota] Update Frota',
  UpdateFrotas = '[Frota] Update Frotas',
  DeleteFrota = '[Frota] Delete Frota',
  DeleteFrotas = '[Frota] Delete Frotas',
  ClearFrotas = '[Frota] Clear Frotas',

  AppendFrota = '[Frota] Append Frota'
}

export class LoadFrotas implements Action {
  readonly type = FrotaActionTypes.LoadFrotas;
}

export class LoadSuccessFrotas implements Action {
  readonly type = FrotaActionTypes.LoadSuccessFrotas;

  constructor(public payload: Frota[]) {}
}

export class LoadFail implements Action {
  readonly type = FrotaActionTypes.LoadFail;

  constructor(public payload: any) {}
}

export class AddFrota implements Action {
  readonly type = FrotaActionTypes.AddFrota;

  constructor(public payload: { frota: Frota }) {}
}

export class UpsertFrota implements Action {
  readonly type = FrotaActionTypes.UpsertFrota;

  constructor(public payload: { frota: Frota }) {}
}

export class AddFrotas implements Action {
  readonly type = FrotaActionTypes.AddFrotas;

  constructor(public payload: { frotas: Frota[] }) {}
}

export class UpsertFrotas implements Action {
  readonly type = FrotaActionTypes.UpsertFrotas;

  constructor(public payload: { frotas: Frota[] }) {}
}

export class UpdateFrota implements Action {
  readonly type = FrotaActionTypes.UpdateFrota;

  constructor(public payload: { frota: Update<Frota> }) {}
}

export class UpdateFrotas implements Action {
  readonly type = FrotaActionTypes.UpdateFrotas;

  constructor(public payload: { frotas: Update<Frota>[] }) {}
}

export class DeleteFrota implements Action {
  readonly type = FrotaActionTypes.DeleteFrota;

  constructor(public payload: { id: string }) {}
}

export class DeleteFrotas implements Action {
  readonly type = FrotaActionTypes.DeleteFrotas;

  constructor(public payload: { ids: string[] }) {}
}

export class ClearFrotas implements Action {
  readonly type = FrotaActionTypes.ClearFrotas;
}

export class AppendFrota implements Action {
  readonly type = FrotaActionTypes.AppendFrota;

  constructor(public payload: Frota ) {}
}

export type FrotaActions =
 | LoadFrotas
 | LoadSuccessFrotas
 | LoadFail
 | AddFrota
 | UpsertFrota
 | AddFrotas
 | UpsertFrotas
 | UpdateFrota
 | UpdateFrotas
 | DeleteFrota
 | DeleteFrotas
 | ClearFrotas
 | AppendFrota;
