import { AgmCoreModule } from '@agm/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { FrotaService } from './resources/frota.service';
import { MaterialModule } from '../shared/third-parts/material';
import { FrotaComponent } from './containers/frota.component';
import { StoreModule } from '@ngrx/store';
import { FrotaEffects } from './effects/frota.effects';
import * as fromFrota from './reducers';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

export const COMPONENTS = [
  FrotaComponent
];

@NgModule({
  imports: [
    CommonModule,
    AgmCoreModule,
    MaterialModule,
    NgxDatatableModule,
    StoreModule.forFeature('frota', fromFrota.reducers),
    EffectsModule.forFeature([FrotaEffects]),
  ],
  declarations: [ COMPONENTS ],
  exports: [ COMPONENTS ],
  providers: [
    FrotaService
  ]
})
export class FrotaModule { }
