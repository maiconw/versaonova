import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Frota } from '../models/frota.model';
import { FrotaActions, FrotaActionTypes, AppendFrota } from '../actions/frota.actions';

export interface State extends EntityState<Frota> {
  selectedFrotaId: string | null;
  loading: boolean;
}

export const adapter: EntityAdapter<Frota> = createEntityAdapter<Frota>({
  selectId: (frota: Frota) => frota.id,
});

export const initialState: State = adapter.getInitialState({
  selectedFrotaId: null,
  loading: false,
});

export function reducer(
  state = initialState,
  action: FrotaActions
): State {
  switch (action.type) {

    case FrotaActionTypes.AppendFrota: {
      return adapter.addOne(action.payload, state);
    }

    case FrotaActionTypes.AddFrota: {
      return adapter.addOne(action.payload.frota, state);
    }

    case FrotaActionTypes.UpdateFrota: {
      return adapter.updateOne(action.payload.frota, state);
    }

    case FrotaActionTypes.DeleteFrota: {
      return adapter.removeOne(action.payload.id, state);
    }

    case FrotaActionTypes.LoadFrotas: {
      return {
        ...state,
        loading: true,
      };
    }

    case FrotaActionTypes.LoadSuccessFrotas: {
      return adapter.addAll(action.payload, state);
    }

    default: {
      return state;
    }
  }
}

export const getSelectedFrotaId = (state: State) => state.selectedFrotaId;

export const {
  // select the array of user ids
  selectIds: getFrotaIds,

  // select the dictionary of user entities
  selectEntities: getFrotaEntities,

  // select the array of users
  selectAll: getAllFrota,

  // select the total user count
  selectTotal: getFrotaTotal
} = adapter.getSelectors();
