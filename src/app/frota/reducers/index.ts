import {
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
} from '@ngrx/store';
import * as fromFrota from './frota.reducer';

export interface FrotaState {
  collection: fromFrota.State;
}

export const reducers: ActionReducerMap<FrotaState> = {
  collection: fromFrota.reducer
};


export const getFrotaState = createFeatureSelector<FrotaState>('frota');

export const getCollectionState = createSelector(
  getFrotaState,
  (state: FrotaState) => state.collection
);

export const getFrotaIds = createSelector(getCollectionState, fromFrota.getFrotaIds);
export const getFrotaEntities = createSelector(getCollectionState, fromFrota.getFrotaEntities);
export const getAllFrota = createSelector(getCollectionState, fromFrota.getAllFrota);
export const getFrotaTotal = createSelector(getCollectionState, fromFrota.getFrotaTotal);
export const getCurrentFrotaId = createSelector(getCollectionState, fromFrota.getSelectedFrotaId);

export const getCurrentFrota = createSelector(
  getFrotaEntities,
  getCurrentFrotaId,
  (frotaEntities, frotaId) => frotaEntities[frotaId]
);
