import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, withLatestFrom, mergeMap, switchMap } from 'rxjs/operators';
import { PositionActions } from '../../map/actions/position.actions';
import { Tracker } from '../../tracker/models/tracker.model';

import { SelectedsActionTypes } from '../actions/selecteds.actions';
import * as fromTracker from '../../tracker/reducers';

import * as fromMap from '../../map/actions/position.actions';
import { SidebarActions } from '../actions/sidebar.actions';
import * as fromSidebar from '../actions/sidebar.actions';

@Injectable()
export class SelectedsEffects {

  constructor(private actions$: Actions, private store: Store<fromTracker.TrackerState>) {}

  @Effect()
  selectBem: Observable<PositionActions|SidebarActions> = this.actions$.pipe(
    ofType(SelectedsActionTypes.SelectBem),
    withLatestFrom( this.store.select( fromTracker.getTrackerBySelectedBem )),
    mergeMap(([action, tracker]) => of(tracker).pipe(
      switchMap((tracker: Tracker ) => [
        new fromMap.SetPositionsMap( {latitude: tracker.latitude, longitude: tracker.longitude, zoom: 16}),
        new fromSidebar.ResetSidebar(),
      ]),
      catchError((error) => {
        return of<PositionActions|SidebarActions>(
          new fromMap.SetPositionsMap( { latitude : -24.282022, longitude : -53.8419715, zoom : 15 } ),
          new fromSidebar.ResetSidebar(),
        );
      })
    )),
  );

  @Effect()
  selectFrota: Observable<PositionActions> = this.actions$.pipe(
    ofType(SelectedsActionTypes.SelectFrota),
    withLatestFrom( this.store.select( fromTracker.getTrackerBySelectedBem )),
    mergeMap(([action, tracker]) => of(tracker).pipe(
      map((tracker: Tracker ) => new fromMap.SetPositionsMap(
        {latitude: tracker.latitude, longitude: tracker.longitude, zoom: 16}
      )),
      catchError((error) => {
        return of( new fromMap.SetPositionsMap( { latitude : -24.282022, longitude : -53.8419715, zoom : 14 } ) )
      })
    )),
  );
}
