import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { switchMap, map, withLatestFrom, mergeMap } from 'rxjs/operators';
import { CollectionActions } from '../../comando/actions/collection.actions';
import { ComandoService } from '../../comando/resources/comando.service';

import * as fromComando from '../../comando/reducers';

import {
  SidebarActions, SidebarActionTypes, BloqueiaBem, BloqueiaBemSuccess, EnvioComandoFailure, ConfiguraSidebar
} from '../actions/sidebar.actions';

@Injectable()
export class SidebarEffects {

  constructor(private actions$: Actions,
              private comandoService: ComandoService,
              private store: Store<fromComando.ComandoState>) {}

  @Effect()
  bloqueiaBem: Observable<SidebarActions> = this.actions$.pipe(
    ofType(SidebarActionTypes.BloqueiaBem),
    map(( action: BloqueiaBem ) => action.payload ),
    switchMap((bemID) =>
      this.comandoService.bloqueia(bemID)
        .then( () => new BloqueiaBemSuccess() )
        .catch( (error) => new EnvioComandoFailure(error.message))
    )
  );

  @Effect()
  resetSidebar: Observable<SidebarActions> = this.actions$.pipe(
    ofType(SidebarActionTypes.ResetSidebar),
    withLatestFrom( this.store.select( fromComando.getStatusButtonsSidebarBySelectedBem )),
    mergeMap(([action, status]) => of(status).pipe(
        map(()=> {
          return new ConfiguraSidebar(status);
        })
    ))
  );
}
