import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Bem } from '../../bem/models/bem.model';
import { Usuario } from '../../login/models/usuario';

import { LogRocketService } from '../../shared/third-parts/logrocket/logrocket.service';
import { Tracker } from '../../tracker/models/tracker.model';

import * as fromMain from '../reducers';
import * as fromLogin from '../../login/reducers';
import * as fromTracker from '../../tracker/reducers';
import * as fromComando from '../../comando/reducers';

import * as layoutActions from '../actions/layout.actions';
import * as sidebarActions from '../actions/sidebar.actions';

@Component({
  selector: 'ot-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainComponent implements OnInit {

  showSidenav$: Observable<boolean>;

  showSidebar$: Observable<boolean>;

  usuarioLogado$: Observable<Usuario>;

  trackerBemSelected$: Observable<Tracker>;

  selectedBem$: Observable<Bem>;

  disableButtonBloqueio$: Observable<boolean>;

  disableButtonDesbloqueio$: Observable<boolean>;

  constructor( private store: Store<fromMain.MainState>, private logRocket: LogRocketService) {
    this.showSidenav$ = this.store.pipe(select(fromMain.getShowSidenav));
    this.showSidebar$ = this.store.pipe(select(fromMain.getShowSidebar));
    this.usuarioLogado$ = this.store.pipe(select(fromLogin.getUser));
    this.trackerBemSelected$ = this.store.pipe(select(fromTracker.getTrackerBySelectedBem));
    this.selectedBem$ = this.store.pipe(select(fromMain.getSelectedBem));
    this.disableButtonBloqueio$ = this.store.pipe(select(fromMain.getBotaoBloquear));
    this.disableButtonDesbloqueio$ = this.store.pipe(select(fromMain.getBotaoDesbloquear));

    this.disableButtonBloqueio$.subscribe((value)=> console.log('BOTAO BLOQUEIO',value))
    this.disableButtonDesbloqueio$.subscribe((value)=> console.log('BOTAO DESBLOQUEIO',value))
  }

  ngOnInit() {
    this.logRocket.identifyUser(this.usuarioLogado$);
  }

  onToogleSideNav() {
    this.store.dispatch(new layoutActions.ToggleSideNav());
  }

  onToogleSideBar() {
    this.store.dispatch(new layoutActions.ToggleSideBar());
  }

  onClosedStart() {
    this.store.dispatch(new layoutActions.CloseSideNav());
  }

  onDesbloqueiaBem( event ) {
    this.store.dispatch(new sidebarActions.DesbloqueiaBem( event ));
  }

  onBloqueiaBem( event ) {
    this.store.dispatch(new sidebarActions.BloqueiaBem( event ));
  }
}
