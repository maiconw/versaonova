export interface SidebarState {
  botaoBloquear?: boolean;
  botaoDesboquear?: boolean;
  comandoEnvioLoading?: boolean;
  execucaoLoading?: boolean;
}

export const initialState: SidebarState = {
  botaoBloquear: true,
  botaoDesboquear: false,
  comandoEnvioLoading: false,
  execucaoLoading: false
};
