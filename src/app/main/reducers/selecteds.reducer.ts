import { Action } from '@ngrx/store';
import { Bem } from '../../bem/models/bem.model';
import { Frota } from '../../frota/models/frota.model';
import { SelectedsActions, SelectedsActionTypes } from '../actions/selecteds.actions';

export interface SelectedsState {
  selectedBem: Bem | null;
  selectedFrota: Frota | null;
}

export const initialState: SelectedsState = {
  selectedBem: null,
  selectedFrota: null
};

export function reducer(
  state = initialState,
  action: SelectedsActions
): SelectedsState {
  switch (action.type) {

    case SelectedsActionTypes.SelectBem: {
      return {
        selectedBem: action.payload,
        selectedFrota: null
      };
    }

    case SelectedsActionTypes.SelectFrota: {
      return {
        selectedBem: null,
        selectedFrota: action.payload
      };
    }

    default:
      return state;
  }
}

export const getSelectedBem = (state: SelectedsState) => state.selectedBem;
export const getSelectedFrota = (state: SelectedsState) => state.selectedFrota;
