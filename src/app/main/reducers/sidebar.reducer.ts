import { SidebarActions, SidebarActionTypes } from '../actions/sidebar.actions';
import { SidebarState, initialState } from './sidebar.state';

export function reducer(
  state: SidebarState = initialState,
  action: SidebarActions
): SidebarState {

  switch (action.type) {

    case SidebarActionTypes.ResetSidebar: {
      return Object.assign( {}, state, {
        botaoBloquear: true,
        botaoDesboquear: false,
        comandoEnvioLoading: false,
        execucaoLoading: false
      } );
    }

    case SidebarActionTypes.ConfiguraSidebar: {
      return Object.assign( {}, state, action.payload );
    }

    case SidebarActionTypes.BloqueiaBem: {
      return Object.assign( {}, state, {
        comandoEnvioLoading: true
      } );
    }

    case SidebarActionTypes.BloqueiaBemSuccess: {
      return Object.assign( {}, state, {
        comandoEnvioLoading: false,
        botaoBloquear: false,
        execucaoLoading: true
      } );
    }

    case SidebarActionTypes.BloqueiaExecutadoSuccess: {
      return Object.assign( {}, state, {
        botaoDesboquear: true,
        execucaoLoading: false
      } );
    }

    case SidebarActionTypes.DesbloqueiaBem: {
      return Object.assign( {}, state, {
        comandoEnvioLoading: true
      } );
    }

    case SidebarActionTypes.DesbloqueiaBemSuccess: {
      return Object.assign( {}, state, {
        botaoDesboquear: false,
        comandoEnvioLoading: false,
        execucaoLoading: true
      } );
    }

    case SidebarActionTypes.DesbloqueiaExecutadoSuccess: {
      return Object.assign( {}, state, {
        botaoBloquear: true,
        execucaoLoading: false
      } );
    }

    case SidebarActionTypes.EnvioComandoFailure: {
      return Object.assign( {}, state, {
        comandoEnvioLoading: false
      } );
    }

    default:
      return state;
  }
}

export const getBotaoBloquear = (state: SidebarState) => state.botaoBloquear;

export const getBotaoDesbloquear = (state: SidebarState) => state.botaoDesboquear;

export const getExecucaoLoading = (state: SidebarState) => state.execucaoLoading;
