import { LayoutActionTypes, LayoutActions } from '../actions/layout.actions';

export interface LayoutState {
  showSidenav: boolean;
  showSidebar: boolean;
}

const initialState: LayoutState = {
  showSidenav: false,
  showSidebar: false,
};

export function reducer(
  state: LayoutState = initialState,
  action: LayoutActions
): LayoutState {

  switch (action.type) {
    case LayoutActionTypes.CloseSidenav: {
      return Object.assign( {}, state, {
        showSidenav: false,
      } );
    }

    case LayoutActionTypes.ToggleSidenav: {
      return Object.assign( {}, state, {
        showSidenav: !state.showSidenav,
      } );
    }

    case LayoutActionTypes.ToggleSidebar: {
      return Object.assign( {}, state, {
        showSidebar: !state.showSidebar,
      } );
    }

    default:
      return state;
  }
}

export const getShowSidenav = (state: LayoutState) => state.showSidenav;

export const getShowSidebar = (state: LayoutState) => state.showSidebar;
