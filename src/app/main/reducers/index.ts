import {
  ActionReducerMap,
  createFeatureSelector,
  createSelector
} from '@ngrx/store';
import { Comando } from '../../comando/models/comando';


import * as fromLayout from './layout';
import * as fromSelecteds from './selecteds.reducer';
import * as fromSidebar from './sidebar.reducer';
import * as fromComando from '../../comando/reducers';

import { SidebarState } from './sidebar.state';

export interface MainState  {
  layout: fromLayout.LayoutState;
  selecteds: fromSelecteds.SelectedsState;
  sidebar: SidebarState;
}

export const reducers: ActionReducerMap<MainState> = {
  layout: fromLayout.reducer,
  selecteds: fromSelecteds.reducer,
  sidebar: fromSidebar.reducer
};

export const getMainState = createFeatureSelector<MainState>('main');

// Sidebar Selectores
export const getSidebarState = createSelector(
  getMainState,
  (state: MainState) => state.sidebar
);


export const getBotaoBloquear = createSelector(
  getSidebarState,
  fromSidebar.getBotaoBloquear
);

export const getBotaoDesbloquear = createSelector(
  getSidebarState,
  fromSidebar.getBotaoDesbloquear
);

// Layout Selectors
export const getLayoutState = createSelector(
  getMainState,
  (state: MainState) => state.layout
);

export const getShowSidenav = createSelector(
  getLayoutState,
  fromLayout.getShowSidenav
);

export const getShowSidebar = createSelector(
  getLayoutState,
  fromLayout.getShowSidebar
);

// Selecteds Selectors
export const getSelectedsState = createSelector(
  getMainState,
  (state: MainState) => state.selecteds
);

export const getSelectedBem = createSelector(
  getSelectedsState,
  fromSelecteds.getSelectedBem
);

export const getSelectedFrota = createSelector(
  getSelectedsState,
  fromSelecteds.getSelectedFrota
);

