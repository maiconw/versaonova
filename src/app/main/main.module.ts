import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { MomentModule } from 'angular2-moment';
import { BemModule } from '../bem/bem.module';
import { ComandoModule } from '../comando/comando.module';
import { ComandoService } from '../comando/resources/comando.service';
import { FrotaComponent } from '../frota/containers/frota.component';
import { FrotaModule } from '../frota/frota.module';
import { AuthGuard } from '../login/guards/auth.guard';
import { MapComponent } from '../map/containers/map.component';
import { MapModule } from '../map/map.module';

import { MaterialModule } from '../shared/third-parts/material';
import { TrackerModule } from '../tracker/tracker.module';

import { LayoutComponent } from './components/layout/layout.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { ContentComponent } from './components/content/content.component';
import { SideNavComponent } from './components/sidenav/sidenav.component';
import { SideBarComponent } from './components/sidebar/sidebar.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { VerticalmenuComponent } from './components/verticalmenu/verticalmenu.component';
import { MainComponent } from './containers/main.component';
import { SelectedsEffects } from './effects/selecteds.effects';
import { SidebarEffects } from './effects/sidebar.effects';

import { reducers } from './reducers';

import * as moment from 'moment';
moment.locale('pt-BR');

export const COMPONENTS = [
  MainComponent,
  ContentComponent,
  DropdownComponent,
  LayoutComponent,
  SideNavComponent,
  SideBarComponent,
  ToolbarComponent,
  VerticalmenuComponent
];

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    MomentModule,
    RouterModule.forChild([
      { path: '', redirectTo: 'main', pathMatch: 'full' },
      {
        path: 'main',
        component: MainComponent,
        canActivate: [ AuthGuard ],
        children: [
          {
            path: '',
            component: MapComponent
          },
          {
            path: 'map',
            component: MapComponent
          },
          {
            path: 'frota',
            component: FrotaComponent
          }
        ],
      }
    ]),
    MapModule,
    FrotaModule,
    BemModule,
    TrackerModule,
    ComandoModule,
    /**
     * StoreModule.forRoot is imported once in the root module, accepting a reducer
     * function or object map of reducer functions. If passed an object of
     * reducers, combineReducers will be run creating your application
     * meta-reducer. This returns all providers for an @ngrx/store
     * based application.
     */
    StoreModule.forFeature('main', reducers ),

    /**
     * EffectsModule.forRoot() is imported once in the root module and
     * sets up the effects class to be initialized immediately when the
     * application starts.
     *
     * See: https://github.com/ngrx/platform/blob/master/docs/effects/api.md#forroot
     */
    EffectsModule.forFeature([SelectedsEffects, SidebarEffects]),


  ],
  declarations: [ COMPONENTS ],
  exports: [ COMPONENTS ]
})
export class MainModule {}
