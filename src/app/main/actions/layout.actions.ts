import { Action } from '@ngrx/store';

export enum LayoutActionTypes {
  CloseSidenav = '[Layout] Close Sidenav',
  ToggleSidenav = '[Layout] Toggle Sidenav',
  ToggleSidebar = '[Layout] Toggle Sidebar',
}

export class ToggleSideNav implements Action {
  readonly type = LayoutActionTypes.ToggleSidenav;
}
export class CloseSideNav implements Action {
  readonly type = LayoutActionTypes.CloseSidenav;
}

export class ToggleSideBar implements Action {
  readonly type = LayoutActionTypes.ToggleSidebar;
}

export type LayoutActions =  CloseSideNav | ToggleSideNav | ToggleSideBar;
