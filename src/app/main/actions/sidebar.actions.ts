import { Action } from '@ngrx/store';
import { SidebarState } from '../reducers/sidebar.state';

export enum SidebarActionTypes {

  ResetSidebar = '[Main Sidebar] ResetSidebar',
  ConfiguraSidebar = '[Main Sidebar] Configura Sidebar para o Bem Selecionado',

  BloqueiaBem = '[Main Sidebar] Bloqueia Bem',
  BloqueiaBemSuccess = '[Main Sidebar] Bloqueio enviado com Successo',
  BloqueiaExecutadoSuccess = '[Main Sidebar] Bloqueio executado com Successo',
  DesbloqueiaBem = '[Main Sidebar] Desbloqueia Bem',
  DesbloqueiaBemSuccess = '[Main Sidebar] Desbloqueio completado com Successo',
  DesbloqueiaExecutadoSuccess = '[Main Sidebar] Desbloqueio executado com Successo',

  EnvioComandoFailure = '[Main Sidebar] Falha ao enviar o comando',
}


export class ResetSidebar implements Action {
  readonly type = SidebarActionTypes.ResetSidebar;
}

export class ConfiguraSidebar implements Action {
  readonly type = SidebarActionTypes.ConfiguraSidebar;

  constructor(public payload: SidebarState) {}
}

export class BloqueiaBem implements Action {
  readonly type = SidebarActionTypes.BloqueiaBem;

  constructor(public payload: string) {}
}

export class BloqueiaBemSuccess implements Action {
  readonly type = SidebarActionTypes.BloqueiaBemSuccess;
}

export class BloqueiaExecutadoSuccess implements Action {
  readonly type = SidebarActionTypes.BloqueiaExecutadoSuccess;
}

export class DesbloqueiaBem implements Action {
  readonly type = SidebarActionTypes.DesbloqueiaBem;

  constructor(public payload: string) {}
}

export class DesbloqueiaBemSuccess implements Action {
  readonly type = SidebarActionTypes.DesbloqueiaBemSuccess;
}

export class DesbloqueiaExecutadoSuccess implements Action {
  readonly type = SidebarActionTypes.DesbloqueiaExecutadoSuccess;
}

export class EnvioComandoFailure implements Action {
  readonly type = SidebarActionTypes.EnvioComandoFailure;

  constructor( public payload: string ){
    console.log(this.payload)
  };
}

export type SidebarActions =
  | ResetSidebar
  | ConfiguraSidebar
  | BloqueiaBem
  | BloqueiaBemSuccess
  | BloqueiaExecutadoSuccess
  | DesbloqueiaBem
  | DesbloqueiaBemSuccess
  | DesbloqueiaExecutadoSuccess
  | EnvioComandoFailure;
