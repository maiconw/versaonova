import { Action } from '@ngrx/store';
import { Bem } from '../../bem/models/bem.model';
import { Frota } from '../../frota/models/frota.model';

export enum SelectedsActionTypes {
  SelectBem = '[Main Select] Seleciona Bem',
  SelectFrota = '[Main Select] Seleciona Frota',
}

export class SelectBem implements Action {
  readonly type = SelectedsActionTypes.SelectBem;

  constructor(public payload: Bem) {}

}
export class SelectFrota implements Action {
  readonly type = SelectedsActionTypes.SelectFrota;

  constructor(public payload: Frota) {}
}

export type SelectedsActions =  SelectBem | SelectFrota;
