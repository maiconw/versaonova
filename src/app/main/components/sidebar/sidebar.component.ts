import { ChangeDetectionStrategy, Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Bem } from '../../../bem/models/bem.model';
import { Tracker } from '../../../tracker/models/tracker.model';

@Component({
  selector: 'ot-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SideBarComponent implements OnInit {

  @Input() selectedBem: Bem;

  @Input() trackerBemSelected: Tracker;

  @Input() disableButtonBloqueio: boolean;

  @Input() disableButtonDesbloqueio: boolean;

  @Output() bloqueiaBem = new EventEmitter();

  @Output() desbloqueiaBem = new EventEmitter();

  constructor() { }

  ngOnInit() {}


}
