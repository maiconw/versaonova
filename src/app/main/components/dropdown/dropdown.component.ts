import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Bem } from '../../../bem/models/bem.model';
import { Frota } from '../../../frota/models/frota.model';

@Component({
  selector: 'ot-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DropdownComponent implements OnInit {

  @Input() frotasCollection: Frota[];
  @Input() bemCollection: Bem[];

  @Input() selectedBem: Bem;
  @Input() selectedFrota: Frota;

  @Output() selectBem  = new EventEmitter();
  @Output() selectFrota  = new EventEmitter();

  constructor() { }

  ngOnInit() {}

}
