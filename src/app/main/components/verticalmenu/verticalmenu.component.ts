import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromMain from '../../reducers';
import * as Auth from '../../../login/actions/auth.actions';

@Component({
  selector: 'ot-verticalmenu',
  templateUrl: './verticalmenu.component.html',
  styleUrls: ['./verticalmenu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VerticalmenuComponent implements OnInit {

  constructor( private store: Store<fromMain.MainState> ) { }

  ngOnInit() {}

  onLogoff() {
    this.store.dispatch(new Auth.Logout());
  }
}
