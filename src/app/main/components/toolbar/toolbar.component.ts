import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Bem } from '../../../bem/models/bem.model';
import { Frota } from '../../../frota/models/frota.model';
import * as fromBem from '../../../bem/reducers';
import * as fromFrota from '../../../frota/reducers';
import * as Main from '../../actions/selecteds.actions';
import * as fromMain from '../../reducers';

@Component({
  selector: 'ot-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolbarComponent {

  @Output() toogleSideBar = new EventEmitter();

  @Output() toogleSideNav = new EventEmitter();

  frotasCollection$: Observable<Frota[]>;

  bemCollection$: Observable<Bem[]>;

  selectedBem$: Observable<Bem>;

  selectedFrota$: Observable<Frota>;

  constructor( private store: Store<fromMain.MainState> ) {
    this.frotasCollection$ = this.store.pipe(select(fromFrota.getAllFrota));
    this.bemCollection$ = this.store.pipe(select(fromBem.getAllBem));
    this.selectedBem$ = this.store.pipe(select(fromMain.getSelectedBem));
    this.selectedFrota$ = this.store.pipe(select(fromMain.getSelectedFrota));
  }

  onSelectBem(event: Bem) {
   this.store.dispatch(new Main.SelectBem(event));
  }

  onSelectFrota(event: Frota) {
    this.store.dispatch(new Main.SelectFrota(event));
  }
}
