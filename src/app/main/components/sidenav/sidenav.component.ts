import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Usuario } from '../../../login/models/usuario';

@Component({
  selector: 'ot-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SideNavComponent {

  @Input() usuario: Usuario;

}
