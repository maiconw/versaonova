
import {
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
} from '@ngrx/store';
import { Bem } from '../../bem/models/bem.model';
import * as fromMain from '../../main/reducers';
import { SidebarState } from '../../main/reducers/sidebar.state';
import { Comando } from '../models/comando';
import * as fromComando from '../reducers/comando.reducer';

export interface ComandoState {
  collection: fromComando.ComandoState;
}

export const reducers: ActionReducerMap<ComandoState> = {
  collection: fromComando.reducer
};


export const getComandoState = createFeatureSelector<ComandoState>('comando');

export const getCollectionState = createSelector(
  getComandoState,
  (state: ComandoState) => state.collection
);

export const getComandoIds = createSelector(getCollectionState, fromComando.getComandoIds);
export const getComandoEntities = createSelector(getCollectionState, fromComando.getComandoEntities);
export const getAllComando = createSelector(getCollectionState, fromComando.getAllComando);
export const getComandoTotal = createSelector(getCollectionState, fromComando.getComandoTotal);
export const getCurrentComandoId = createSelector(getCollectionState, fromComando.getSelectedComandoId);

export const getCurrentComando = createSelector(
  getComandoEntities,
  getCurrentComandoId,
  (comandoEntities, comandoId) => comandoEntities[comandoId]
);

export const getComandoBySelectedBem = createSelector(fromMain.getSelectedBem, getAllComando, (selectedBem: Bem, allEntities) => {
  if (allEntities && selectedBem){
    return allEntities.filter((entitie) =>  entitie.bem_id === selectedBem.id )
  } else {
    return [];
  }
});

export const getStatusButtonsSidebarBySelectedBem = createSelector(
  getComandoBySelectedBem,
  (entities: Comando[]): SidebarState => {
    if (entities.length) {
      let codComandos = [];

      //Retorna todos os valores que contem os comandos 143(Bloqueio)  e 144(Desbloqueio)
      let collection = entities.filter( ( entitie ) => entitie.cod_comando == '143' || entitie.cod_comando == '144' );

      //Retorna somente os Codigos
      collection.map((comando)=> codComandos.push(comando.cod_comando) );

      //Retira os Codigos Duplicados
      codComandos = codComandos.filter( (value, index, array) => {return array.indexOf(value) == index });

      if (  codComandos[0] == '143' ){
        return { botaoBloquear: false, botaoDesboquear: false, execucaoLoading: true }
      }else{
        return { botaoBloquear: false, botaoDesboquear: false, execucaoLoading: true }
      }
    }
  }
);
