import { EntityState, createEntityAdapter, EntityAdapter } from '@ngrx/entity';
import { CollectionActions, CollectionActionsTypes } from '../actions/collection.actions';
import { Comando } from '../models/comando';

export interface ComandoState extends EntityState<Comando> {
  selectedComandoId: string | null;
  loading: boolean;
}


export const adapter: EntityAdapter<Comando> = createEntityAdapter<Comando>({
  selectId: (comando: Comando) => comando.id
});

export const initialState: ComandoState = adapter.getInitialState({
  selectedComandoId: null,
  loading: false,
});

export function reducer(
  state: ComandoState = initialState,
  action: CollectionActions
): ComandoState {

  switch (action.type) {
    case CollectionActionsTypes.LoadComandos: {
      return {
        ...state,
        loading: true,
      };
    }

    case CollectionActionsTypes.LoadComandosSuccess: {
      return adapter.addAll(action.payload, state);
    }

    default:
      return state;
  }
}

export const getSelectedComandoId = (state: ComandoState) => state.selectedComandoId;

export const {
  // select the array of user ids
  selectIds: getComandoIds,

  // select the dictionary of user entities
  selectEntities: getComandoEntities,

  // select the array of users
  selectAll: getAllComando,

  // select the total user count
  selectTotal: getComandoTotal
} = adapter.getSelectors();
