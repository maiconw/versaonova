import { Action } from '@ngrx/store';
import { Comando } from '../models/comando';

export enum CollectionActionsTypes {
  LoadComandos = '[Comando] Carrega comandos',
  LoadComandosSuccess = '[Comando] Comandos carregados com successo',
  LoadComandosFailure = '[Comando] Carregamento dos comandos falhou'
}

export class LoadComandos implements Action {
  readonly type =  CollectionActionsTypes.LoadComandos;
}

export class LoadComandosSuccess implements Action {
  readonly type = CollectionActionsTypes.LoadComandosSuccess;

  constructor( public payload: Comando[]) {}
}

export class LoadComandosFailure implements Action {
  readonly type = CollectionActionsTypes.LoadComandosFailure;

  constructor( public payload: string ) {}
}

export type CollectionActions =
  | LoadComandos
  | LoadComandosSuccess
  | LoadComandosFailure;
