import { TestBed, inject } from '@angular/core/testing';

import { ComandoService } from './comando.service';

describe('ComandoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ComandoService]
    });
  });

  it('should be created', inject([ComandoService], (service: ComandoService) => {
    expect(service).toBeTruthy();
  }));
});
