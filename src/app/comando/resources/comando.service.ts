import { Injectable } from '@angular/core';
import { SocketService } from '../../shared/third-parts/featherjs/socket.service';

@Injectable()
export class ComandoService {

  serviceName = 'comando';

  constructor( private _socket: SocketService ) {};

  getAllComandos() {
    const collectionStringIDS: Array<string> = this._socket.getApp().get('user')['bens'];

    return this._socket.getService('comando').find({
      query: {
        bem_id: {$in: collectionStringIDS}
      }
    });
  }


  bloqueia(bemId: string) {
    return this._socket.getService(this.serviceName).create({
      comando: '143',
      bem_id: bemId
    });
  }


  desbloqueia(bemId: string) {
    return this._socket.getService(this.serviceName).create({
      comando: '144',
      bem_id: bemId
    });
  }

}
