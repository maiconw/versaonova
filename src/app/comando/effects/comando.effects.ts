import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { switchMap } from 'rxjs/operators';
import { CollectionActions, CollectionActionsTypes, LoadComandosFailure, LoadComandosSuccess } from '../actions/collection.actions';
import { Comando } from '../models/comando';
import { ComandoService } from '../resources/comando.service';

@Injectable()
export class ComandoEffects {

  constructor(private actions$: Actions, private comandoService: ComandoService) {}

  @Effect()
  loadTracker: Observable<CollectionActions> = this.actions$.pipe(
    ofType(CollectionActionsTypes.LoadComandos),
    switchMap(() =>
      this.comandoService.getAllComandos()
        .then( (comandos: Comando[]) => new LoadComandosSuccess(comandos['data']))
        .catch( (error) => new LoadComandosFailure(error.message))
    )
  );
}
