export interface Comando {
  id: string;
  cod_comando: string;
  comando: string;
  bem_id: string;
  descricao: string;
  motivo: string;
  dt_envio: number;
  dt_executado: number;
  executado: boolean;
}
