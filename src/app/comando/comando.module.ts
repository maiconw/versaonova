import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ComandoEffects } from './effects/comando.effects';
import { ComandoService } from './resources/comando.service';

import * as fromComando from './reducers';

@NgModule({
  imports: [
    StoreModule.forFeature('comando', fromComando.reducers),
    EffectsModule.forFeature([ComandoEffects])
  ],
  declarations: [],
  providers: [
    ComandoService
  ]
})
export class ComandoModule { }
